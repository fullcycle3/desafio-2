const express = require("express");
const mysql = require("mysql");
const faker = require("faker-br");

const app = express();
const PORT = 3000;

const config = {
  host: "db",
  user: "root",
  password: "password",
  database: "nodedb",
};

const connection = mysql.createConnection(config);

app.get("/", async (req, res) => {
  const insert = `INSERT INTO people(name) values('${faker.name.firstName()} ${faker.name.lastName()}')`;
  connection.query(insert);

  const sql = `SELECT id, name FROM people`;

  connection.query(sql, (error, results) => {
    if (error) {
      throw error;
    }

    let list = "<ul>";
    for (let people of results) {
      list += `<li>${people.name}</li>`;
    }

    list += "</ul>";
    res.send("<h1>Full Cycle Rocks!</h1>" + list);
  });
});

app.listen(PORT, () => {
  console.log("Server running on " + PORT);
});
